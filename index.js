const http = require("http");

const server = http.createServer((req, res) => {
  res.write('hai-v1');
  res.end();
});

const PORT = 80 || process.env['PORT'];

server.listen(PORT, () => {
  process.stdout.write('server listen on port ' + PORT);
});
