import * as express from 'express';
import { createServer } from 'http'
import * as cors from "cors";
import { Server } from "net";
import { json as jsonBodyParser } from 'body-parser';

const port = 3000
const app = express()

app.set('port',port)
app.use(cors)
app.get('/',jsonBodyParser(), function(){
    console.log('test')
})

const server = createServer(app);

export function startServer(): Server {
    return server.listen(port, () => {
      console.log("server listen on port ", port);
    });
  }